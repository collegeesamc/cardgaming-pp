﻿using pp1_cardgaming.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace pp1_cardgaming
{
    public partial class DeckForm : Form
    {
        

        private Monstro monstro = new Monstro();
        private Deck myDeck;
        
        //Classe para trabalhar com o json
        WorkWithJson wwJson = new WorkWithJson();

        public DeckForm()
        {   
            InitializeComponent();
            configTableDeck();

        }

        private void configTableDeck()
        {
            myDeck = wwJson.getConfigDeck();
            if (myDeck == null)
                myDeck = new Deck();

            setValueInTable(myDeck.monstros);
           
        }

        private void setValueInTable(List<Monstro> monstros)
        {
            int i = 0;
            dataGridView1.Rows.Clear();
            foreach (Monstro dk in monstros)
            {

                dataGridView1.Rows.Add(i,dk.imageMonstro.nome, dk.danoFisico, dk.danoMagico, dk.defesaFisica, dk.defesaMagica);
                i++;
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            wwJson.writeConfigDeck(this.myDeck);
            MessageBox.Show("Salvo com sucesso");
        }
      

        private void DeckForm_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            UtilDeck ud = new UtilDeck();
            myDeck = ud.generatedDeckAleatory();
            setValueInTable(myDeck.monstros);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pp1_cardgaming.Model
{
    public class Slot
    {
        public Monstro monstro { get; set; }
        public bool firstInField { get; set; }
        public bool liberado { get; set; }
        public Slot()
        {
            firstInField = true;
            liberado = true;
        }
        public Slot(Monstro monstro)
        {
            this.monstro = monstro;
            liberado = false;
        }

        public String getText()
        {
            return "Dano Fisico: " + monstro.danoFisico
                + "\nDano Magico: " + monstro.danoMagico
                + "\nDefesa Fisica: " + monstro.defesaFisica
                + "\nDefesa Magica: " + monstro.defesaMagica;
        }

    }
}

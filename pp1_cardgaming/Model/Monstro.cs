﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pp1_cardgaming
{
    public class ImageMonstro
    {
        public string nome { get; set; }
        public string url { get; set; }
    }
    public class Monstro
    {

        public ImageMonstro imageMonstro { get; set; }
        public int danoFisico { get; set; }
        public int danoMagico { get; set; }
        public int defesaFisica { get; set; }
        public int defesaMagica { get; set; }
        
        public Monstro()
        {
           
        }

        public int CalcPoints()
        {
            return (danoFisico + danoMagico + defesaFisica + defesaMagica);
        }

        public string Values
        {
            get
            {
                return "danoFisico: " + danoFisico + " - danoMagico:" + danoMagico + 
                    " - defesaFisica:" + defesaFisica + " - defesaMagica:" + defesaMagica +
                    "Nome: "+imageMonstro.nome+ " - url: "+imageMonstro.url;
            }
        }
    }
}

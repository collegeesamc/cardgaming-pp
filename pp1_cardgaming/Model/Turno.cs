﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pp1_cardgaming.Model
{
    public class Turno
    {
        
        public Player atacantePlayer;
        public Player recebedorPlayer;
        public int selectedMonster { get;set; }
        public int getDeck { get; set; }

        public Game game;
        public Turno(Game game)
        {
            this.game = game;
        }


        public void ataqueMonstro(int monstroAt,int monstroRec)
        {
            Monstro atacante = atacantePlayer.cardBattle[monstroAt].monstro;
            Monstro recebedor = recebedorPlayer.cardBattle[monstroRec].monstro;

            if (atacante.danoFisico > recebedor.defesaFisica)
            {
                atacante.danoFisico -= recebedor.defesaFisica;
                removeEnemy(monstroAt, monstroRec);
            }
            else if(atacante.danoMagico > recebedor.defesaMagica)
            {
                atacante.danoMagico -= recebedor.defesaMagica;
                removeEnemy(monstroAt, monstroRec);
            }
            else
            {
                recebedor.defesaMagica -= atacante.danoMagico;
                recebedor.defesaFisica -= atacante.danoFisico;
                removeEnemyMyMonster(monstroAt, monstroRec);
            }
        }

        public void ataqueMonstroByBot(int monstroAt, int monstroRec)
        {
            Monstro atacante = atacantePlayer.cardBattle[monstroAt].monstro;
            Monstro recebedor = recebedorPlayer.cardBattle[monstroRec].monstro;

            if (atacante.danoFisico > recebedor.defesaFisica)
            {
                atacante.danoFisico -= recebedor.defesaFisica;
                removeEnemyMyMonster(monstroAt, monstroRec);
            }
            else if (atacante.danoMagico > recebedor.defesaMagica)
            {
                atacante.danoMagico -= recebedor.defesaMagica;
                removeEnemyMyMonster(monstroAt, monstroRec);
            }
            else
            {
                recebedor.defesaMagica -= atacante.danoMagico;
                recebedor.defesaFisica -= atacante.danoFisico;
                removeEnemy(monstroAt, monstroRec);
            }
        }
        private void removeEnemy(int mstAt,int mstRec)
        {
            recebedorPlayer.cardBattle[mstRec] = null;
            game.getPictureBoxBotBattle(mstRec).Image = null;
            game.setTextInSlotBattle(mstAt, atacantePlayer.cardBattle[mstAt].getText());
            game.setTextInBattleBot(mstRec, "");
        }
        private void removeEnemyMyMonster(int mstAt,int mstRec)
        {
            atacantePlayer.cardBattle[mstAt] = null;
            game.getBattle(mstAt).Image = null;
            game.setTextInSlotBattle(mstAt, "");
            game.setTextInBattleBot(mstRec, recebedorPlayer.cardBattle[mstRec].getText());
        }
        

        

      

    }
}

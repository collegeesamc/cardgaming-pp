﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pp1_cardgaming
{
    public class Deck
    {
        public Deck()
        {
            
        }
        public string name { get; set; }

        public List<Monstro> monstros { get; set; }

        public void addMonstro(Monstro monstro)
        {
            if (monstros == null)
                monstros = new List<Monstro>();

                monstros.Add(monstro);
        }

        public int countCard()
        {
            return monstros.Count;
        }
    }
}

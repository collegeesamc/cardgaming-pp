﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pp1_cardgaming.Model
{
    public class Player
    {
        
        public int life { get; set; }
        public Deck deck { get; set; }
        public Slot[]  slot = new Slot[5];
        public Slot[] cardBattle = new Slot[5];

        public Player()
        {
            
        }
    }
}

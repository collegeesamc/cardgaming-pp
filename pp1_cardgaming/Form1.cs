﻿using pp1_cardgaming.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pp1_cardgaming
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_deck_Click(object sender, EventArgs e)
        {
            var form = new DeckForm();
            form.Show();
        }


        private bool verificaInicioDoJogo()
        {
            WorkWithJson wwJson = new WorkWithJson();
            Deck myDeck = wwJson.getConfigDeck();
            if (myDeck != null)
            {
                if (myDeck.countCard() < 25)
                {
                    MessageBox.Show("Você não terminou de finalizar o deck");
                    return false;
                }
            }
            else
                MessageBox.Show("Você não criou o deck ainda");

            return true;

        }
        private void btn_iniciarjogo_Click(object sender, EventArgs e)
        {
            if (verificaInicioDoJogo())
            {
                new Game().Show();
            }
        }
    }
}

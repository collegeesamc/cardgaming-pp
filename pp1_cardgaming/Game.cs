﻿using pp1_cardgaming.Model;
using pp1_cardgaming.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pp1_cardgaming
{
    public partial class Game : Form
    {
        public Turno jogo;

        public Player player;

        public Player bot;

        public const string urlImageMonstro = "Monstros/";

        Random rdn = new Random();

        public Game()
        {
            InitializeComponent();

            this.player = new Player();
            this.bot = new Player();


            jogo = new Turno(this);
            WorkWithJson wwJson = new WorkWithJson();
            //Carregando o deck do jogador
            player.deck = wwJson.getConfigDeck();
            player.life = ConstantsGame.MAXLIFE;
            //Carregando o deck do bot
            bot.deck = wwJson.getConfigBotDeck();
            bot.life = ConstantsGame.MAXLIFE;

            //Configurações iniciais do player
            configPlayer(player);

            //Incluindo os atributos no texto do player
            for (int i = 0; i < player.slot.Length; i++)
                loadInSlot(i, player.slot[i]);

            //Configurando o bot
            configPlayer(bot);

            //Iniciando o turno para o player
            jogo.atacantePlayer = player;
            jogo.recebedorPlayer = bot;
            jogo.getDeck = 0;
            MessageBox.Show("Seu turno");
        }

        /**
         * <summary>Configurações inicial dos jogadores</summary>
         * 
         */ 
        private void configPlayer(Player player)
        {
            
            int number = rdn.Next(player.deck.monstros.Count);
            for (int i = 0; i < player.slot.Length; i++)
            {
                player.slot[i] = new Slot();
                number = rdn.Next(player.deck.monstros.Count);
                player.slot[i].monstro = player.deck.monstros.ElementAt(number);
                //Coloca o slot para não receber carta nova
                player.slot[i].liberado = false;
            }
        }


        /**
         * <summary>Passa o turno o atacante é quem tera o turno</summary>
         * <param name="atacantePlayer">De qual player sera o turno</param>
         * <param name="recebedor">Qual player está passando o turno</param>
         */
        public void setTurno(Player atacantePlayer,Player recebedor)
        {
            jogo.atacantePlayer = atacantePlayer;
            jogo.recebedorPlayer = recebedor;
            jogo.getDeck = 0;
            jogo.selectedMonster = -1;
            foreach (Slot sl in atacantePlayer.cardBattle)
            {
                if(sl != null)
                {
                    sl.firstInField = false;
                }
            }

            Console.WriteLine(verificaVencedor(player));
            if (verificaVencedor(player) == 0)
            {
                MessageBox.Show("Você Perdeu");
                this.Close();
                    
            }
            
            if (verificaVencedor(bot) == 0)
            {
                MessageBox.Show("Você Venceu");
                this.Close();
            }

        }


        public int verificaVencedor(Player player)
        {
            int contador = 0;

            foreach (Slot sl in player.cardBattle)
                if (sl != null)
                    contador++;
            foreach (Slot sl in player.slot)
                if (!sl.liberado)
                    contador++;

            contador += player.deck.countCard();
            return contador;
        }


        /**
         * <summary>Carrega o monstro retira do deck principal inclui no slot carregando a imagem e o texto</summary>
         * <param name="attrSlot">A string para incluir na label</param>
         * <param name="index">Numero do slot qual slot vai ser trocado</param>
         */ 
        private void loadInSlot(int index,Slot attrSlot)
        {
            player.deck.monstros.Remove(player.slot[index].monstro);
            getSlot(index).Load(urlImageMonstro + attrSlot.monstro.imageMonstro.url);
            setTextInSlot(index, attrSlot.getText());
        }


        /**
         * <summary>Inclui um monstro do slot para o slot de batalha do bot</summary>
         * <param name="i">Campo de batalha a ser selecionado</param>
         * <param name="slot">O slot que ira sair a carta será liberada para ser possivel incluir novas cartas</param>
         */
        public void loadInBattleBot(int i,Slot slot)
        {
            
            bot.cardBattle[i] = new Slot(slot.monstro);
            string urlMonster = (urlImageMonstro + slot.monstro.imageMonstro.url);
            slot.liberado = true;
            setTextInBattleBot(i, bot.cardBattle[i].getText());
            getPictureBoxBotBattle(i).Load(urlMonster);
        }

        /**
         * <summary>Inclui um texto no campo de batalha do bot</summary>
         */
        public void setTextInBattleBot(int index,string value)
        {
            switch (index)
            {
                case 0:
                    txtBotAttrBattle1.Text = value;
                    break;
                case 1:
                    txtBotAttrBattle2.Text = value;
                    break;
                case 2:
                    txtBotAttrBattle3.Text = value;
                    break;
                case 3:
                    txtBotAttrBattle4.Text = value;
                    break;
                case 4:
                    txtBotAttrBattle5.Text = value;
                    break;
            }
        }

        public PictureBox getPictureBoxBotBattle(int index)
        {
            switch (index)
            {
                case 0:
                    return pictureBattleBot1;
                    
                case 1:
                    return pictureBattleBot2;
                    
                case 2:
                    return pictureBattleBot3;
                    
                case 3:
                    return pictureBattleBot4;
                case 4:
                    return pictureBattleBot5;
            }
            return null;
        }


        /**
         * <summary>Inclui os valores nos texto do slot</summary>
         */
        private void setTextInSlot(int index,string value)
        {
            switch (index)
            {
                case 0:
                    txtSlotAttr1.Text = value;
                    break;
                case 1:
                    txtSlotAttr2.Text = value;
                    break;
                case 2:
                    txtSlotAttr3.Text = value;
                    break;
                case 3:
                    txtSlotAttr4.Text = value;
                    break;
                case 4:
                    txtSlotAttr5.Text = value;
                    break;
            }
        }

        /**
        * <summary>Inclui os valores nos texto do slot de batalha</summary>
        * <param name="index">Qual text ira muda ro valor</param>
        * <param name="value">Valor a ser passado</param>
        */
        public void setTextInSlotBattle(int index, string value)
        {
            switch (index)
            {
                case 0:
                    textAttrBattle1.Text = value;
                    break;
                case 1:
                    textAttrBattle2.Text = value;
                    break;
                case 2:
                    textAttrBattle3.Text = value;
                    break;
                case 3:
                    textAttrBattle4.Text = value;
                    break;
                case 4:
                    textAttrBattle5.Text = value;
                    break;
            }
        }

        /**
         *<summary>Retorna o PictureBox do campo de slot</summary>
         */
        private PictureBox getSlot(int index)
        {
            switch (index)
            {
                case 0:
                    return pictureSlot1;
                case 1:
                    return pictureSlot2;
                case 2:
                    return pictureSlot3;
                case 3:
                    return pictureSlot4;
                case 4:
                    return pictureSlot5;
            }
            return null;
        }

        /**
         * <summary>Todos os campos de batalha</summary>
         * <returns>Retorna o atributo do campo</returns>
         */
        public PictureBox getBattle(int index)
        {
            switch (index)
            {
                case 0:
                    return pictureBattle1;
                case 1:
                    return pictureBattle2;
                case 2:
                    return pictureBattle3;
                case 3:
                    return pictureBattle4;
                case 4:
                    return pictureBattle5;
            }
            return null;
        }

        /**
         * <summary>Botão de deck ao clicar incluir novo monstro no slot</summary>
         */
        private void button1_Click(object sender, EventArgs e)
        {
            if (jogo.atacantePlayer == player)
            {
                if (jogo.getDeck < ConstantsGame.MAXGETCARDDECK)
                {
                    int number = 0;
                    if (player.deck.monstros.Count > 0)
                    {
                        bool recebeCarta = false;
                        for (int i = 0; i < player.slot.Length; i++)
                        {
                            if (player.slot[i].liberado)
                            {
                                jogo.getDeck++;
                                number = rdn.Next(player.deck.monstros.Count);
                                player.slot[i].monstro = player.deck.monstros.ElementAt(number);
                                loadInSlot(i, player.slot[i]);
                                recebeCarta = true;
                                player.slot[i].liberado = false;
                                txt_cartas.Text = "Cartas: " + player.deck.countCard();
                                break;
                            }
                        }
                        if (!recebeCarta)
                            MessageBox.Show("Seu slot já está cheio");
                    }
                    else
                    {
                        MessageBox.Show("Você não tem cartas suficientes");
                    }
                }
                else
                {
                    MessageBox.Show("Apenas uma carta por turno");
                }
            }
        }

        /**
         * <summary>Carrega o novo mosntro para a batalha e reseta os valores antigos do mesmo</summary>
         * <param name="i">Qual slot de batalha será incluido</param>
         * <param name="index">Qual o index do slot a incluir para resetar</param>
         * <param name="slot">O novo monstro para incluir </param>
         */
        private void loadInBattle(int i,int index, Slot slot)
        {
            getBattle(i).ImageLocation = getSlot(index).ImageLocation;
            getSlot(index).Image = null;
            player.cardBattle[i] = new Slot(slot.monstro);
            slot.liberado = true;
            setTextInSlot(index, "");
            setTextInSlotBattle(i, slot.getText());
        }

        /**
         * <summary>A ação do stot para colocar o monstro de batalha</summary>
         * 
         * <param name="slot">Passa o slot clicado</param>
         * <param name="index">Qual picture box resetar</param>
         */
        private void ActionSlot(Slot slot,int index)
        {
            if(jogo.atacantePlayer == player)
            { 
                var confirmResult = MessageBox.Show("Deseja incluir a carta no campo de batalha ? "
                        , "Incluir Carta", MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    for(int i=0; i < player.cardBattle.Length; i++)
                    {
                        //Verifica se existe algum campo livre para jogar
                        if(player.cardBattle[i] == null)
                        {
                            getBattle(i).ImageLocation = getSlot(index).ImageLocation;
                            getSlot(index).Image = null;
                            player.cardBattle[i] = new Slot(slot.monstro);
                            //Adiciona o slot clicado para um campo de batalha
                            //Liberando o slot para ser possivel colocar uma nova cart
                            slot.liberado = true;

                            loadInBattle(i, index, slot);
                            break;
                        }
                    }
                }
            }
        }

        /**
         * <summary>Prepara o monstro para atacar o monstro inimigo</summary>
         * <param name="index">Para o index selecionado</param>
         */
        private void actionMonsterBattle(int index)
        {
            if(jogo.atacantePlayer == player)
            {
                if(player.cardBattle[index] != null)
                {
                    
                    if(!player.cardBattle[index].firstInField)
                    {
                        
                        jogo.selectedMonster = index;
                        txtSelectedMoster.Text = player.cardBattle[index].monstro.imageMonstro.nome;
                        Console.WriteLine("Selecionando monstro: "+ player.cardBattle[index].monstro.imageMonstro.nome);
                    }
                }
            }else
            {
                MessageBox.Show("Não é sua vez");
            }
        }


        private void pictureSlot1_Click(object sender, EventArgs e)
        {
            if (!player.slot[0].liberado)
            {
                ActionSlot(player.slot[0], 0);
            }
        }

        private void pictureSlot2_Click(object sender, EventArgs e)
        {
            if (!player.slot[1].liberado)
            {
                ActionSlot(player.slot[1], 1);
            }
        }

        private void pictureSlot3_Click(object sender, EventArgs e)
        {
            if (!player.slot[2].liberado)
            {
                ActionSlot(player.slot[2], 2);
            }
        }

        private void pictureSlot4_Click(object sender, EventArgs e)
        {
            if (!player.slot[3].liberado)
            {
                ActionSlot(player.slot[3], 3);
            }
        }

        private void pictureSlot5_Click(object sender, EventArgs e)
        {
            if (!player.slot[4].liberado)
            {
                ActionSlot(player.slot[4], 4);
            }
        }

        private void pictureBattle1_Click(object sender, EventArgs e)
        {
            actionMonsterBattle(0);
        }

        private void pictureBattle2_Click(object sender, EventArgs e)
        {
            actionMonsterBattle(1);
        }

        private void pictureBattle3_Click(object sender, EventArgs e)
        {
            actionMonsterBattle(2);
        }

        private void pictureBattle4_Click(object sender, EventArgs e)
        {
            actionMonsterBattle(3);
        }

        private void pictureBattle5_Click(object sender, EventArgs e)
        {
            actionMonsterBattle(4);
        }

        private void txt_life_Click(object sender, EventArgs e)
        {

        }

        private void btn_passarTurno_Click(object sender, EventArgs e)
        {
            if(jogo.atacantePlayer == player)
            {

                setTurno(bot, player);
                Thread.Sleep(50);
                MessageBox.Show("Passando turno para o bot");
                new ScriptBot().ExecBot(this);
            }
            else
            {
                MessageBox.Show("Não é seu turno para passar");
            }
        }

        private void atacarMostro(int index)
        {
            if(jogo.atacantePlayer == player)
            {
                if(jogo.selectedMonster != -1)
                {
                    if(player.cardBattle[jogo.selectedMonster] != null)
                    { 
                        Console.WriteLine("Monstro: " + player.cardBattle[jogo.selectedMonster]
                            .monstro.imageMonstro.nome);
                        jogo.ataqueMonstro(jogo.selectedMonster, index);
                    }else
                    {
                        MessageBox.Show("Selecione um monstro do campo de batalha");
                    }
                }
            }
        }


        private void pictureBattleBot1_Click(object sender, EventArgs e)
        {
            if(bot.cardBattle[0] != null)
                atacarMostro(0);
        }

        private void pictureBattleBot2_Click(object sender, EventArgs e)
        {
            if (bot.cardBattle[1] != null)
                atacarMostro(1);
        }

        private void pictureBattleBot3_Click(object sender, EventArgs e)
        {
            if (bot.cardBattle[2] != null)
                atacarMostro(2);
        }

        private void pictureBattleBot4_Click(object sender, EventArgs e)
        {
            if (bot.cardBattle[3] != null)
                atacarMostro(3);
        }

        private void pictureBattleBot5_Click(object sender, EventArgs e)
        {
            if (bot.cardBattle[4] != null)
                atacarMostro(4);
        }


        //FIm do botao do slot

    }
}

﻿namespace pp1_cardgaming
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn_deck = new System.Windows.Forms.Button();
            this.btn_iniciarjogo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_deck
            // 
            this.btn_deck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(5)))), ((int)(((byte)(61)))), ((int)(((byte)(138)))));
            this.btn_deck.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_deck.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_deck.Location = new System.Drawing.Point(120, 140);
            this.btn_deck.Name = "btn_deck";
            this.btn_deck.Size = new System.Drawing.Size(75, 23);
            this.btn_deck.TabIndex = 0;
            this.btn_deck.Text = "Criar Deck";
            this.btn_deck.UseVisualStyleBackColor = false;
            this.btn_deck.Click += new System.EventHandler(this.btn_deck_Click);
            // 
            // btn_iniciarjogo
            // 
            this.btn_iniciarjogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(5)))), ((int)(((byte)(61)))), ((int)(((byte)(138)))));
            this.btn_iniciarjogo.FlatAppearance.BorderSize = 0;
            this.btn_iniciarjogo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_iniciarjogo.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_iniciarjogo.Location = new System.Drawing.Point(120, 90);
            this.btn_iniciarjogo.Name = "btn_iniciarjogo";
            this.btn_iniciarjogo.Size = new System.Drawing.Size(75, 23);
            this.btn_iniciarjogo.TabIndex = 1;
            this.btn_iniciarjogo.Text = "Iniciar Jogo";
            this.btn_iniciarjogo.UseVisualStyleBackColor = false;
            this.btn_iniciarjogo.Click += new System.EventHandler(this.btn_iniciarjogo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(308, 232);
            this.Controls.Add(this.btn_iniciarjogo);
            this.Controls.Add(this.btn_deck);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Card Game";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_deck;
        private System.Windows.Forms.Button btn_iniciarjogo;
    }
}


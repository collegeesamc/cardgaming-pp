﻿namespace pp1_cardgaming
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_cartas = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSlotAttr5 = new System.Windows.Forms.Label();
            this.txtSlotAttr4 = new System.Windows.Forms.Label();
            this.txtSlotAttr3 = new System.Windows.Forms.Label();
            this.txtSlotAttr2 = new System.Windows.Forms.Label();
            this.txtSlotAttr1 = new System.Windows.Forms.Label();
            this.pictureSlot5 = new System.Windows.Forms.PictureBox();
            this.pictureSlot4 = new System.Windows.Forms.PictureBox();
            this.pictureSlot2 = new System.Windows.Forms.PictureBox();
            this.pictureSlot3 = new System.Windows.Forms.PictureBox();
            this.pictureSlot1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_life = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textAttrBattle5 = new System.Windows.Forms.Label();
            this.textAttrBattle4 = new System.Windows.Forms.Label();
            this.textAttrBattle3 = new System.Windows.Forms.Label();
            this.textAttrBattle2 = new System.Windows.Forms.Label();
            this.textAttrBattle1 = new System.Windows.Forms.Label();
            this.pictureBattle1 = new System.Windows.Forms.PictureBox();
            this.pictureBattle5 = new System.Windows.Forms.PictureBox();
            this.pictureBattle4 = new System.Windows.Forms.PictureBox();
            this.pictureBattle3 = new System.Windows.Forms.PictureBox();
            this.pictureBattle2 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtBotAttrBattle5 = new System.Windows.Forms.Label();
            this.txtBotAttrBattle4 = new System.Windows.Forms.Label();
            this.txtBotAttrBattle3 = new System.Windows.Forms.Label();
            this.txtBotAttrBattle2 = new System.Windows.Forms.Label();
            this.txtBotAttrBattle1 = new System.Windows.Forms.Label();
            this.pictureBattleBot1 = new System.Windows.Forms.PictureBox();
            this.pictureBattleBot5 = new System.Windows.Forms.PictureBox();
            this.pictureBattleBot4 = new System.Windows.Forms.PictureBox();
            this.pictureBattleBot3 = new System.Windows.Forms.PictureBox();
            this.pictureBattleBot2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSelectedMoster = new System.Windows.Forms.Label();
            this.btn_passarTurno = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle2)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            this.panel1.Controls.Add(this.txt_cartas);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.txt_life);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(19, 459);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(679, 231);
            this.panel1.TabIndex = 0;
            // 
            // txt_cartas
            // 
            this.txt_cartas.AutoSize = true;
            this.txt_cartas.ForeColor = System.Drawing.Color.Cyan;
            this.txt_cartas.Location = new System.Drawing.Point(599, 19);
            this.txt_cartas.Name = "txt_cartas";
            this.txt_cartas.Size = new System.Drawing.Size(43, 13);
            this.txt_cartas.TabIndex = 5;
            this.txt_cartas.Text = "Cartas: ";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtSlotAttr5);
            this.panel2.Controls.Add(this.txtSlotAttr4);
            this.panel2.Controls.Add(this.txtSlotAttr3);
            this.panel2.Controls.Add(this.txtSlotAttr2);
            this.panel2.Controls.Add(this.txtSlotAttr1);
            this.panel2.Controls.Add(this.pictureSlot5);
            this.panel2.Controls.Add(this.pictureSlot4);
            this.panel2.Controls.Add(this.pictureSlot2);
            this.panel2.Controls.Add(this.pictureSlot3);
            this.panel2.Controls.Add(this.pictureSlot1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(20, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(571, 200);
            this.panel2.TabIndex = 4;
            // 
            // txtSlotAttr5
            // 
            this.txtSlotAttr5.AutoSize = true;
            this.txtSlotAttr5.Location = new System.Drawing.Point(460, 119);
            this.txtSlotAttr5.Name = "txtSlotAttr5";
            this.txtSlotAttr5.Size = new System.Drawing.Size(22, 13);
            this.txtSlotAttr5.TabIndex = 12;
            this.txtSlotAttr5.Text = "attr";
            // 
            // txtSlotAttr4
            // 
            this.txtSlotAttr4.AutoSize = true;
            this.txtSlotAttr4.Location = new System.Drawing.Point(347, 119);
            this.txtSlotAttr4.Name = "txtSlotAttr4";
            this.txtSlotAttr4.Size = new System.Drawing.Size(22, 13);
            this.txtSlotAttr4.TabIndex = 11;
            this.txtSlotAttr4.Text = "attr";
            // 
            // txtSlotAttr3
            // 
            this.txtSlotAttr3.AutoSize = true;
            this.txtSlotAttr3.Location = new System.Drawing.Point(235, 119);
            this.txtSlotAttr3.Name = "txtSlotAttr3";
            this.txtSlotAttr3.Size = new System.Drawing.Size(22, 13);
            this.txtSlotAttr3.TabIndex = 10;
            this.txtSlotAttr3.Text = "attr";
            // 
            // txtSlotAttr2
            // 
            this.txtSlotAttr2.AutoSize = true;
            this.txtSlotAttr2.Location = new System.Drawing.Point(120, 119);
            this.txtSlotAttr2.Name = "txtSlotAttr2";
            this.txtSlotAttr2.Size = new System.Drawing.Size(22, 13);
            this.txtSlotAttr2.TabIndex = 9;
            this.txtSlotAttr2.Text = "attr";
            // 
            // txtSlotAttr1
            // 
            this.txtSlotAttr1.AutoSize = true;
            this.txtSlotAttr1.Location = new System.Drawing.Point(8, 119);
            this.txtSlotAttr1.Name = "txtSlotAttr1";
            this.txtSlotAttr1.Size = new System.Drawing.Size(22, 13);
            this.txtSlotAttr1.TabIndex = 8;
            this.txtSlotAttr1.Text = "attr";
            // 
            // pictureSlot5
            // 
            this.pictureSlot5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureSlot5.Location = new System.Drawing.Point(463, 16);
            this.pictureSlot5.Name = "pictureSlot5";
            this.pictureSlot5.Size = new System.Drawing.Size(100, 100);
            this.pictureSlot5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureSlot5.TabIndex = 7;
            this.pictureSlot5.TabStop = false;
            this.pictureSlot5.Click += new System.EventHandler(this.pictureSlot5_Click);
            // 
            // pictureSlot4
            // 
            this.pictureSlot4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureSlot4.Location = new System.Drawing.Point(350, 16);
            this.pictureSlot4.Name = "pictureSlot4";
            this.pictureSlot4.Size = new System.Drawing.Size(100, 100);
            this.pictureSlot4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureSlot4.TabIndex = 6;
            this.pictureSlot4.TabStop = false;
            this.pictureSlot4.Click += new System.EventHandler(this.pictureSlot4_Click);
            // 
            // pictureSlot2
            // 
            this.pictureSlot2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureSlot2.Location = new System.Drawing.Point(123, 16);
            this.pictureSlot2.Name = "pictureSlot2";
            this.pictureSlot2.Size = new System.Drawing.Size(100, 100);
            this.pictureSlot2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureSlot2.TabIndex = 5;
            this.pictureSlot2.TabStop = false;
            this.pictureSlot2.Click += new System.EventHandler(this.pictureSlot2_Click);
            // 
            // pictureSlot3
            // 
            this.pictureSlot3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureSlot3.Location = new System.Drawing.Point(238, 16);
            this.pictureSlot3.Name = "pictureSlot3";
            this.pictureSlot3.Size = new System.Drawing.Size(100, 100);
            this.pictureSlot3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureSlot3.TabIndex = 4;
            this.pictureSlot3.TabStop = false;
            this.pictureSlot3.Click += new System.EventHandler(this.pictureSlot3_Click);
            // 
            // pictureSlot1
            // 
            this.pictureSlot1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureSlot1.Image = ((System.Drawing.Image)(resources.GetObject("pictureSlot1.Image")));
            this.pictureSlot1.Location = new System.Drawing.Point(11, 16);
            this.pictureSlot1.Name = "pictureSlot1";
            this.pictureSlot1.Size = new System.Drawing.Size(100, 100);
            this.pictureSlot1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureSlot1.TabIndex = 3;
            this.pictureSlot1.TabStop = false;
            this.pictureSlot1.Click += new System.EventHandler(this.pictureSlot1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Cyan;
            this.label1.Location = new System.Drawing.Point(18, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Minhas cartas";
            // 
            // txt_life
            // 
            this.txt_life.AutoSize = true;
            this.txt_life.ForeColor = System.Drawing.Color.Cyan;
            this.txt_life.Location = new System.Drawing.Point(316, 0);
            this.txt_life.Name = "txt_life";
            this.txt_life.Size = new System.Drawing.Size(90, 13);
            this.txt_life.TabIndex = 3;
            this.txt_life.Text = "Pontos de vida: 0";
            this.txt_life.Click += new System.EventHandler(this.txt_life_Click);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(598, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 136);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(5)))), ((int)(((byte)(61)))), ((int)(((byte)(138)))));
            this.label3.ForeColor = System.Drawing.Color.Cyan;
            this.label3.Location = new System.Drawing.Point(21, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Turno: ";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Maroon;
            this.panel3.Controls.Add(this.textAttrBattle5);
            this.panel3.Controls.Add(this.textAttrBattle4);
            this.panel3.Controls.Add(this.textAttrBattle3);
            this.panel3.Controls.Add(this.textAttrBattle2);
            this.panel3.Controls.Add(this.textAttrBattle1);
            this.panel3.Controls.Add(this.pictureBattle1);
            this.panel3.Controls.Add(this.pictureBattle5);
            this.panel3.Controls.Add(this.pictureBattle4);
            this.panel3.Controls.Add(this.pictureBattle3);
            this.panel3.Controls.Add(this.pictureBattle2);
            this.panel3.Location = new System.Drawing.Point(19, 260);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(679, 199);
            this.panel3.TabIndex = 2;
            // 
            // textAttrBattle5
            // 
            this.textAttrBattle5.AutoSize = true;
            this.textAttrBattle5.Location = new System.Drawing.Point(493, 116);
            this.textAttrBattle5.Name = "textAttrBattle5";
            this.textAttrBattle5.Size = new System.Drawing.Size(22, 13);
            this.textAttrBattle5.TabIndex = 17;
            this.textAttrBattle5.Text = "attr";
            // 
            // textAttrBattle4
            // 
            this.textAttrBattle4.AutoSize = true;
            this.textAttrBattle4.Location = new System.Drawing.Point(384, 116);
            this.textAttrBattle4.Name = "textAttrBattle4";
            this.textAttrBattle4.Size = new System.Drawing.Size(22, 13);
            this.textAttrBattle4.TabIndex = 16;
            this.textAttrBattle4.Text = "attr";
            // 
            // textAttrBattle3
            // 
            this.textAttrBattle3.AutoSize = true;
            this.textAttrBattle3.Location = new System.Drawing.Point(278, 116);
            this.textAttrBattle3.Name = "textAttrBattle3";
            this.textAttrBattle3.Size = new System.Drawing.Size(22, 13);
            this.textAttrBattle3.TabIndex = 15;
            this.textAttrBattle3.Text = "attr";
            // 
            // textAttrBattle2
            // 
            this.textAttrBattle2.AutoSize = true;
            this.textAttrBattle2.Location = new System.Drawing.Point(162, 116);
            this.textAttrBattle2.Name = "textAttrBattle2";
            this.textAttrBattle2.Size = new System.Drawing.Size(22, 13);
            this.textAttrBattle2.TabIndex = 14;
            this.textAttrBattle2.Text = "attr";
            // 
            // textAttrBattle1
            // 
            this.textAttrBattle1.AutoSize = true;
            this.textAttrBattle1.Location = new System.Drawing.Point(43, 116);
            this.textAttrBattle1.Name = "textAttrBattle1";
            this.textAttrBattle1.Size = new System.Drawing.Size(22, 13);
            this.textAttrBattle1.TabIndex = 13;
            this.textAttrBattle1.Text = "attr";
            // 
            // pictureBattle1
            // 
            this.pictureBattle1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattle1.Location = new System.Drawing.Point(46, 13);
            this.pictureBattle1.Name = "pictureBattle1";
            this.pictureBattle1.Size = new System.Drawing.Size(100, 100);
            this.pictureBattle1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattle1.TabIndex = 8;
            this.pictureBattle1.TabStop = false;
            this.pictureBattle1.Click += new System.EventHandler(this.pictureBattle1_Click);
            // 
            // pictureBattle5
            // 
            this.pictureBattle5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattle5.Location = new System.Drawing.Point(496, 13);
            this.pictureBattle5.Name = "pictureBattle5";
            this.pictureBattle5.Size = new System.Drawing.Size(100, 100);
            this.pictureBattle5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattle5.TabIndex = 12;
            this.pictureBattle5.TabStop = false;
            this.pictureBattle5.Click += new System.EventHandler(this.pictureBattle5_Click);
            // 
            // pictureBattle4
            // 
            this.pictureBattle4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattle4.Location = new System.Drawing.Point(387, 13);
            this.pictureBattle4.Name = "pictureBattle4";
            this.pictureBattle4.Size = new System.Drawing.Size(100, 100);
            this.pictureBattle4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattle4.TabIndex = 11;
            this.pictureBattle4.TabStop = false;
            this.pictureBattle4.Click += new System.EventHandler(this.pictureBattle4_Click);
            // 
            // pictureBattle3
            // 
            this.pictureBattle3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattle3.Location = new System.Drawing.Point(281, 13);
            this.pictureBattle3.Name = "pictureBattle3";
            this.pictureBattle3.Size = new System.Drawing.Size(100, 100);
            this.pictureBattle3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattle3.TabIndex = 10;
            this.pictureBattle3.TabStop = false;
            this.pictureBattle3.Click += new System.EventHandler(this.pictureBattle3_Click);
            // 
            // pictureBattle2
            // 
            this.pictureBattle2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattle2.Location = new System.Drawing.Point(165, 13);
            this.pictureBattle2.Name = "pictureBattle2";
            this.pictureBattle2.Size = new System.Drawing.Size(100, 100);
            this.pictureBattle2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattle2.TabIndex = 9;
            this.pictureBattle2.TabStop = false;
            this.pictureBattle2.Click += new System.EventHandler(this.pictureBattle2_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Maroon;
            this.panel4.Controls.Add(this.txtBotAttrBattle5);
            this.panel4.Controls.Add(this.txtBotAttrBattle4);
            this.panel4.Controls.Add(this.txtBotAttrBattle3);
            this.panel4.Controls.Add(this.txtBotAttrBattle2);
            this.panel4.Controls.Add(this.txtBotAttrBattle1);
            this.panel4.Controls.Add(this.pictureBattleBot1);
            this.panel4.Controls.Add(this.pictureBattleBot5);
            this.panel4.Controls.Add(this.pictureBattleBot4);
            this.panel4.Controls.Add(this.pictureBattleBot3);
            this.panel4.Controls.Add(this.pictureBattleBot2);
            this.panel4.Location = new System.Drawing.Point(19, 20);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(679, 199);
            this.panel4.TabIndex = 3;
            // 
            // txtBotAttrBattle5
            // 
            this.txtBotAttrBattle5.AutoSize = true;
            this.txtBotAttrBattle5.Location = new System.Drawing.Point(493, 116);
            this.txtBotAttrBattle5.Name = "txtBotAttrBattle5";
            this.txtBotAttrBattle5.Size = new System.Drawing.Size(35, 13);
            this.txtBotAttrBattle5.TabIndex = 17;
            this.txtBotAttrBattle5.Text = "label4";
            // 
            // txtBotAttrBattle4
            // 
            this.txtBotAttrBattle4.AutoSize = true;
            this.txtBotAttrBattle4.Location = new System.Drawing.Point(384, 116);
            this.txtBotAttrBattle4.Name = "txtBotAttrBattle4";
            this.txtBotAttrBattle4.Size = new System.Drawing.Size(35, 13);
            this.txtBotAttrBattle4.TabIndex = 16;
            this.txtBotAttrBattle4.Text = "label4";
            // 
            // txtBotAttrBattle3
            // 
            this.txtBotAttrBattle3.AutoSize = true;
            this.txtBotAttrBattle3.Location = new System.Drawing.Point(278, 116);
            this.txtBotAttrBattle3.Name = "txtBotAttrBattle3";
            this.txtBotAttrBattle3.Size = new System.Drawing.Size(35, 13);
            this.txtBotAttrBattle3.TabIndex = 15;
            this.txtBotAttrBattle3.Text = "label4";
            // 
            // txtBotAttrBattle2
            // 
            this.txtBotAttrBattle2.AutoSize = true;
            this.txtBotAttrBattle2.Location = new System.Drawing.Point(162, 116);
            this.txtBotAttrBattle2.Name = "txtBotAttrBattle2";
            this.txtBotAttrBattle2.Size = new System.Drawing.Size(35, 13);
            this.txtBotAttrBattle2.TabIndex = 14;
            this.txtBotAttrBattle2.Text = "label4";
            // 
            // txtBotAttrBattle1
            // 
            this.txtBotAttrBattle1.AutoSize = true;
            this.txtBotAttrBattle1.Location = new System.Drawing.Point(43, 116);
            this.txtBotAttrBattle1.Name = "txtBotAttrBattle1";
            this.txtBotAttrBattle1.Size = new System.Drawing.Size(35, 13);
            this.txtBotAttrBattle1.TabIndex = 13;
            this.txtBotAttrBattle1.Text = "label4";
            // 
            // pictureBattleBot1
            // 
            this.pictureBattleBot1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattleBot1.Location = new System.Drawing.Point(46, 13);
            this.pictureBattleBot1.Name = "pictureBattleBot1";
            this.pictureBattleBot1.Size = new System.Drawing.Size(100, 100);
            this.pictureBattleBot1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattleBot1.TabIndex = 8;
            this.pictureBattleBot1.TabStop = false;
            this.pictureBattleBot1.Click += new System.EventHandler(this.pictureBattleBot1_Click);
            // 
            // pictureBattleBot5
            // 
            this.pictureBattleBot5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattleBot5.Location = new System.Drawing.Point(496, 13);
            this.pictureBattleBot5.Name = "pictureBattleBot5";
            this.pictureBattleBot5.Size = new System.Drawing.Size(100, 100);
            this.pictureBattleBot5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattleBot5.TabIndex = 12;
            this.pictureBattleBot5.TabStop = false;
            this.pictureBattleBot5.Click += new System.EventHandler(this.pictureBattleBot5_Click);
            // 
            // pictureBattleBot4
            // 
            this.pictureBattleBot4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattleBot4.Location = new System.Drawing.Point(387, 13);
            this.pictureBattleBot4.Name = "pictureBattleBot4";
            this.pictureBattleBot4.Size = new System.Drawing.Size(100, 100);
            this.pictureBattleBot4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattleBot4.TabIndex = 11;
            this.pictureBattleBot4.TabStop = false;
            this.pictureBattleBot4.Click += new System.EventHandler(this.pictureBattleBot4_Click);
            // 
            // pictureBattleBot3
            // 
            this.pictureBattleBot3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattleBot3.Location = new System.Drawing.Point(281, 13);
            this.pictureBattleBot3.Name = "pictureBattleBot3";
            this.pictureBattleBot3.Size = new System.Drawing.Size(100, 100);
            this.pictureBattleBot3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattleBot3.TabIndex = 10;
            this.pictureBattleBot3.TabStop = false;
            this.pictureBattleBot3.Click += new System.EventHandler(this.pictureBattleBot3_Click);
            // 
            // pictureBattleBot2
            // 
            this.pictureBattleBot2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBattleBot2.Location = new System.Drawing.Point(165, 13);
            this.pictureBattleBot2.Name = "pictureBattleBot2";
            this.pictureBattleBot2.Size = new System.Drawing.Size(100, 100);
            this.pictureBattleBot2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBattleBot2.TabIndex = 9;
            this.pictureBattleBot2.TabStop = false;
            this.pictureBattleBot2.Click += new System.EventHandler(this.pictureBattleBot2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 232);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Monstro Selecionado:";
            // 
            // txtSelectedMoster
            // 
            this.txtSelectedMoster.AutoSize = true;
            this.txtSelectedMoster.Location = new System.Drawing.Point(487, 232);
            this.txtSelectedMoster.Name = "txtSelectedMoster";
            this.txtSelectedMoster.Size = new System.Drawing.Size(0, 13);
            this.txtSelectedMoster.TabIndex = 5;
            // 
            // btn_passarTurno
            // 
            this.btn_passarTurno.Location = new System.Drawing.Point(586, 227);
            this.btn_passarTurno.Name = "btn_passarTurno";
            this.btn_passarTurno.Size = new System.Drawing.Size(112, 23);
            this.btn_passarTurno.TabIndex = 6;
            this.btn_passarTurno.Text = "Passar o turno";
            this.btn_passarTurno.UseVisualStyleBackColor = true;
            this.btn_passarTurno.Click += new System.EventHandler(this.btn_passarTurno_Click);
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(729, 692);
            this.Controls.Add(this.btn_passarTurno);
            this.Controls.Add(this.txtSelectedMoster);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Game";
            this.Text = "Jogo";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureSlot1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattle2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBattleBot2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txt_life;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label txt_cartas;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureSlot1;
        private System.Windows.Forms.PictureBox pictureSlot5;
        private System.Windows.Forms.PictureBox pictureSlot4;
        private System.Windows.Forms.PictureBox pictureSlot2;
        private System.Windows.Forms.PictureBox pictureSlot3;
        private System.Windows.Forms.PictureBox pictureBattle1;
        private System.Windows.Forms.PictureBox pictureBattle5;
        private System.Windows.Forms.PictureBox pictureBattle4;
        private System.Windows.Forms.PictureBox pictureBattle3;
        private System.Windows.Forms.PictureBox pictureBattle2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBattleBot1;
        private System.Windows.Forms.PictureBox pictureBattleBot5;
        private System.Windows.Forms.PictureBox pictureBattleBot4;
        private System.Windows.Forms.PictureBox pictureBattleBot3;
        private System.Windows.Forms.PictureBox pictureBattleBot2;
        private System.Windows.Forms.Label txtSlotAttr5;
        private System.Windows.Forms.Label txtSlotAttr4;
        private System.Windows.Forms.Label txtSlotAttr3;
        private System.Windows.Forms.Label txtSlotAttr2;
        private System.Windows.Forms.Label txtSlotAttr1;
        private System.Windows.Forms.Label textAttrBattle5;
        private System.Windows.Forms.Label textAttrBattle4;
        private System.Windows.Forms.Label textAttrBattle3;
        private System.Windows.Forms.Label textAttrBattle2;
        private System.Windows.Forms.Label textAttrBattle1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtSelectedMoster;
        private System.Windows.Forms.Button btn_passarTurno;
        private System.Windows.Forms.Label txtBotAttrBattle5;
        private System.Windows.Forms.Label txtBotAttrBattle4;
        private System.Windows.Forms.Label txtBotAttrBattle3;
        private System.Windows.Forms.Label txtBotAttrBattle2;
        private System.Windows.Forms.Label txtBotAttrBattle1;
    }
}
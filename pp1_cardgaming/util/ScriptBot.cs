﻿using pp1_cardgaming.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pp1_cardgaming.util
{
    public class ScriptBot
    {

        Game game;
        Random rdn = new Random();
        public void ExecBot(Game game)
        {
            this.game = game;
            includeMosterInBattle(this.game.bot);
            getCardInDeck();
            game.setTurno(game.player, game.bot);
            ataqueMonster();
        }

        private void getCardInDeck()
        {
            Player bot = game.bot;
            if(bot.deck.countCard() > 0)
            {
                if(game.jogo.getDeck < ConstantsGame.MAXGETCARDDECK)
                {
                    if (bot.deck.monstros.Count > 0)
                    {
                        int number = 0;
                        for (int i = 0; i < bot.slot.Length; i++)
                        {
                            if (bot.slot[i].liberado)
                            {
                                game.jogo.getDeck++;
                                number = rdn.Next(bot.deck.monstros.Count);
                                bot.slot[i].monstro = bot.deck.monstros.ElementAt(number);
                                bot.deck.monstros.Remove(bot.slot[i].monstro);
                                bot.slot[i].liberado = false;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void ataqueMonster()
        {
            Player bot = game.bot;
            Player player = game.player;

            for(int i= 0; i < bot.cardBattle.Length;i++)
            {
                if(bot.cardBattle[i] != null) { 
                    for(int x=0; x < player.cardBattle.Length; x++)
                    {
                        if(player.cardBattle[x] != null)
                        {
                            game.jogo.ataqueMonstroByBot(x, i);
                            break;
                        }
                    }
                }
            }
        }

        private void includeMosterInBattle(Player bot)
        {
            Console.Write("Bot: Incluir monstros do slot para o campo de batalha");
            for(int x = 0; x < bot.slot.Length; x++) {
                if (!bot.slot[x].liberado) { 
                    for(int i =0; i < bot.cardBattle.Length; i++)
                    {
                        if(bot.cardBattle[i] == null)
                        {
                            game.loadInBattleBot(i, bot.slot[x]);
                            break;
                        }
                    }
                }
            }

        }
    }
}

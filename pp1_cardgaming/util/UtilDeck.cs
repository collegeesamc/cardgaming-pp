﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pp1_cardgaming.util
{
    public class UtilDeck
    {
        Random oRND = new Random();
        public Deck generatedDeckAleatory()
        {
            Deck deck = new pp1_cardgaming.Deck();
            //Carregando a lista de monstros
            WorkWithJson wwJson = new WorkWithJson();
            List<ImageMonstro> listImage = wwJson.getImagesMonstro();
            int totalMonstro = listImage.Count-1;

            for (int i =0; i < ConstantsGame.MAXCARD; i++)
            {
                Monstro monstro = new Monstro();
                monstro = generatedPointMonstro(monstro);
                monstro.imageMonstro = listImage.ElementAt(oRND.Next(totalMonstro + 1));
                Console.WriteLine(monstro.Values);
                deck.addMonstro(monstro);
            }
            return deck;
        }
        
        public Monstro generatedPointMonstro(Monstro monstro)
        {
            int point = ConstantsGame.MAXPOINT;
            Console.WriteLine(point);
            bool distribuido = true;

            int contador = 0;
            //Enquanto não tiver distribuido os pontos continua
            while(point > 0)
            {
                //Recebe um numero aleatorio baseado no maximo de pontos
                distribuido = true;
                int value = oRND.Next(point+1);
                /*Verifica se o valor foi maior que 10
                 * Se for maior que 10
                 * Passa o valor de 10 para o monstro e tira 10 dos pontos
                 */ 
                if (value >= 10)
                {
                    point -= 5;
                    value = 5;
                }else
                {
                    point -= value;
                }
                
                switch (contador)
                {
                    case 0:
                        if (verificaPoint(monstro.danoFisico, value))
                            monstro.danoFisico += value;
                        else
                            distribuido = false;
                        break;


                    case 1:
                        if (verificaPoint(monstro.danoMagico, value))
                            monstro.danoMagico += value;
                        else
                            distribuido = false;
                        break;

                    case 2:
                        if(verificaPoint(monstro.defesaFisica, value))
                            monstro.defesaFisica += value;
                        else
                            distribuido = false;
                        break;
                    case 3:
                        if (verificaPoint(monstro.defesaMagica, value))
                            monstro.defesaMagica += value;
                        else
                            distribuido = false;
                        break;

                    default:
                        if ((point+value) > 0)
                            point += value;

                        contador = 0;
                        break;
                }
                //Caso não tenha sido distribuido o ponto ele retorna
                if (!distribuido)
                    point += value;
                contador++;
            }

            return monstro;
        }

        public bool verificaPoint(int valor,int rnd)
        {
            return (valor + rnd) <= 10;
        }
    }
}

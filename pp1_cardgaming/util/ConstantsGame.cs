﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pp1_cardgaming.util
{
    public static class ConstantsGame
    {
        public const int MAXPOINT = 20;
        public const int MAXCARD = 25;
        public const int MAXLIFE = 20;
        public const int MAXGETCARDDECK = 1;
    }
}

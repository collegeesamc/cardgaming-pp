﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace pp1_cardgaming.util
{
    public class WorkWithJson
    {
        private static string urlConfigCard = "mycards.json";

        private static string urlConfigImage = "CardMonstros.json";
        private static string urlConfigCardBot = "botcards.json";

        public void writeConfigDeck(Deck deck)
        {
            string deckToDatabase = JsonConvert.SerializeObject(deck);
            Console.WriteLine(deckToDatabase);
            File.WriteAllText(urlConfigCard,deckToDatabase);
        }

        public Deck getConfigDeck()
        {
            //Verifica se foi possivel ler o deck se não da um erro
            using (StreamReader file = File.OpenText(urlConfigCard))
            {
                JsonSerializer serializer = new JsonSerializer();
                Deck deck = (Deck)serializer.Deserialize(file, typeof(Deck));
                return deck;
            }
        }

        public Deck getConfigBotDeck()
        {
            //Verifica se foi possivel ler o deck se não da um erro
            using (StreamReader file = File.OpenText(urlConfigCardBot))
            {
                JsonSerializer serializer = new JsonSerializer();
                Deck deck = (Deck)serializer.Deserialize(file, typeof(Deck));
                return deck;
            }
        }

        public List<ImageMonstro> getImagesMonstro()
        {
            using (StreamReader file = File.OpenText(urlConfigImage))
            {
                JsonSerializer serializer = new JsonSerializer();
                List<ImageMonstro> imageMonstro = (List<ImageMonstro>)serializer.Deserialize(file, typeof(List<ImageMonstro>));
                return imageMonstro;
            }
        }
    }
}
